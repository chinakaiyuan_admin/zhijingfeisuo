广告时间:兜售域名:weixin.pet;namenode.cn

<p align="center">
    <h1 align="center">飞梭智纺</h1>
</p>
 
## 当前食用方式
##### 修改composer.json

##### 增加

```
  "repositories": [
        {
            "type": "git",
            "url": "https://gitee.com/chinakaiyuan_admin/zhijingfeisuo.git"
        }
    ],
```

##### 运行

composer require chinakaiyuan/zhijingfeisuo

## 环境要求

- PHP 7.1+
- Composer

## 实例化

```php {4-23}
use zhijingfeisuo\Application;

$config = [
        'clientId' => "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        'orgCode'  => "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        'loginAccount' => "XXXXXX",
        'password'     => "XXXXXX",
        'log'      => [
            'enable' => false,
            'path'   => '/home/ZhiJing.log',
        ],
];

$app = new Application($config);
```

## 方法说明

```php
$app->Menu->getMenuList();//返回菜单列表
$app->Menu->getOrgList();//返回组织

$app->Craft->query();//返回工艺列表
$app->Craft->craftCompose();//返回工艺明细

$app->Device->query();//返回设备列表
$app->Device->configInfo();//返回设备参数

$app->WorkOrder->delete();//删除已下推订单
$app->WorkOrder->get();//返回订单信息
$app->WorkOrder->icCardScan();//排缸前的扫卡
$app->WorkOrder->saveOrUpdate();//保存/下推/运行 已下推缸订单
$app->WorkOrder->info();//根据iccard，返回排缸单信息bussinesStatus 除了未生产时是0，其他状态下都是20。盲猜接口，不安全，可能随时会被删除
```

### 核心说明

```php
/*
 * @property \zhijingfeisuo\Kernel\Services\AccessToken $access_token
 * @property \zhijingfeisuo\Kernel\Services\Cacher $cacher
 * @property \zhijingfeisuo\Kernel\Services\Logger $logger
 * @property \zhijingfeisuo\Kernel\Services\Interceptor $interceptor
*/
//1、AccessToken 获取token，刷新token
//2、Cacher      缓存token，日志到本地，默认驱动是文件驱动，替换自定义驱动方法如下：
#### 定义缓存驱动方法
class selfCaccher {
    public function set($key,$value,$ttl=0){
        ...
    }
      public function get($key,$value){
        ....
    }
}
//通过这个方法可以将token 等数据保存到服务器
$this->Cacher->setCacher(new selfCaccher);

//3、Logger  日志驱动。保存提交数据和返回数据。默认驱动文件驱动，通过配置文件路径保存，目录必须允许读写。替换自定义驱动方法如下：
#### 定义日志驱动方法
class selfLogger {
    public function debug($key,$value){
        ...
    }

}
$this->Cacher->setLogger(new selfLogger);

//Interceptor 拦截模块，在提交前拦截增加参数，提交后拦截，判断返回值，是否需要重新提交。一般不需要修改。
```

# 初始化举例

```php
/**
 * 通过数据库保存token信息
 */
use zhijingfeisuo\Application;
use zhijingfeisuo\Kernel\Interfaces\CacheInterface;

class ZhiJingApi extends Application implements CacheInterface
    {
    public $defaultconfig = [

        'clientId' => "XXXXXXXXX",
        'orgCode'  => "XXXXXXXXX",
        'loginAccount' => "XXXXXX",
        'password'     => "XXXXXX",
        'log'      => [
            'enable' => true,//开启日志
            'path'   => '/home/log/ZhiJing.log',
        ],
    ];
    //获取token  cache
    public function get($name, $default = null)
        {

        $data = null;

        if ($name == 'zhijing_access_token.userId') {
            $data =//通过数据库获取
            }
        else if ($name == 'zhijing_access_token.accessToken') {
            $data =//通过数据库获取
            }
        else if ($name == 'zhijing_access_token.ciamToken') {
            $data = //通过数据库获取
            }
        else if ($name == 'zhijing_access_token.refreshToken') {
            $data =//通过数据库获取
            }
        else if ($name == 'zhijing_access_token.accessTokenExpire') {
            $data = //通过数据库获取
            }
        else if ($name == 'zhijing_access_token.ciamTokenExpire') {
            $data = //通过数据库获取
            }
        else if ($name == 'zhijing_access_token.refreshTokenExpire') {
            $data =  //通过数据库获取
            }
       ;

        return $data ?? false;
        }
    //设置token cache
    public function set($name, $value, $time = null)
        {
        if ($name == 'zhijing_access_token.userId') {
            //通过数据库保存
            }
        else if ($name == 'zhijing_access_token.accessToken') {
            //通过数据库保存
            }
        else if ($name == 'zhijing_access_token.ciamToken') {
            //通过数据库保存
            }
        else if ($name == 'zhijing_access_token.refreshToken') {
           //通过数据库保存
            }

        }

   public function debug($key,$value){
            //通过数据库保存日志
    }
    public function __construct($options = [])
        {
        parent::__construct(array_merge($this->defaultconfig, $options));
        $this->cacher->setCache($this);
        $this->Cacher->setLogger($this);
        }

    }
    ......

     $Application = new ZhiJingApi($options);
     $res= $Application->WorkOrder->info(订单号);
```

## 下推生产订单 设备参数设置

```php
     function setdevice_value(&$config = [], $valueParam = [])
        {

        foreach ($config as $k => $v) {
            foreach ($valueParam as $kk => $vv) {
                if ($v['paramCode'] == $kk) {
                    $config[$k]['value'] = $vv;
                    continue;
                    }
                }
            }
        }
        ... ...

       //$device_id 设备ID $dyeing_machine_model_code 设备代码
        $device_params = $Application->Device->configInfo(设备ID, $dyeing_machine_model_code);//获取设备运行参数
        $machineConfigDTO = $device_params['vatConfig'];
        $valueParam   = [
            //布重..称重
            'weightNum'          => $receive_quantity,
            //浴比
            'bathRatio'          => $ratio,
            //吸水率
            'bibulousRate'       => $rate,
            //长度
            "length"             => 0,
            //主泵速度
            "mainPumpSpeed"      => 0,
            //喷嘴位置
            "theNozzlePosition"  => 0,
            //提布速度
            "tibSpeed"           => 0,
            //风机速度
            "theFanSpeedControl" => 0,
            //加料水量
            "feedWaterVolume"    => 0,
            //水洗倍数
            "washingTimes"       => 1.5,
            //循环时间
            "forTime"            => 0,
            //最少水量
            "minAmoutOfWater"    => 0,
            //风机开启
            "fanOn"              => 0,
        ];
        $this->setdevice_value($machineConfigDTO, $valueParam);//设置设备运行参数
        //扫卡
        $scan_data = $Application->WorkOrder->icCardScan(订单号, 设备ID);
        $icCardDTO = [$scan_data['data']];

        $orderDTO = [
            'batchNo'         => $scan_data['data']['batchNo'],
            'calicoName'      => $scan_data['data']['fabricName'],
            'clientName'      => $scan_data['data']['customerName'],
            'gramWeight'      => 0,
            'numAndColor'     => $scan_data['data']['color'],
            'patchStatus'     => $scan_data['data']['patchStatus'],
            'repair'          => $scan_data['data']['repair'],
            'whetherTheFirst' => 1
        ];
        //工艺$rscode 染色订单号
        $craft_list = $Application->Craft->query(工艺代码);
        $craft_data = $craft_list['data'][0];

        //工艺明细
        // public function craftCompose($craftIdList, $deviceId, $icCardCode, $icCardWithCraft = false, $code = '', $machineCode = '')
        $craft_items = $Application->Craft->craftCompose($craft_data['id'], 设备ID,流转卡号);

        $craft_items['data']['craftId']     = $craft_items['data']['id'];
        $craft_items['data']['version']     = $craft_items['data']['currentVersion'];
        $craft_items['data']['crafVersion'] = $craft_items['data']['currentVersion'];
        $craft_items['data']['craftCode']   = $craft_items['data']['code'];
        $craft_items['data']['stepList']    = [];

        $workOrderCraftDTO = [$craft_items['data']];

        $workOrderDTO = [
            'createUserId'       => $Application->cacher->get('userId'),
            'deviceId'           => $gdata['device_id'],
            'fabricNameCustomer' => "",
            'lengthNum'          => 0,
            'theSampleColor'     => "#{$rgb}",
            'weightNum'          => $scan_data['data']['planOutputQty']
        ];
        //下推运行工艺
        $Application->WorkOrder->saveOrUpdate($machineConfigDTO, $orderDTO, $workOrderCraftDTO, $workOrderDTO, $icCardDTO, [], [], [], 1, 0);


```
