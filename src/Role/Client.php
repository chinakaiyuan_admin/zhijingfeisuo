<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-23 15:14:57
 * @Description: 工艺接口
 * 
 */

namespace zhijingfeisuo\Role;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {

    public function list()
        {
        return $this->_GET('dyeing-web/base/role/list');
        }

    }