<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-18 14:04:07
 * @Description: 工艺接口
 * 
 */

namespace zhijingfeisuo\Device;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {
    public function query(
        $dyeingCyGroupNo = '',
        $dyeingCylinderTypeCode = '',
        $dyeingMachineModelCode = '',
        $iotSerialNumber = "",
        $pipelineNumber = "",
        $remark = '',
        $stationNumber = 1,
        $status = "",
        $typeCode = "",
        $workshopId = "",
        $workshopName = ""
    ) {
        return $this->postJson('dyeing-nest-control/base/device/query', [
            'dyeingCyGroupNo'        => $dyeingCyGroupNo,
            'dyeingCylinderTypeCode' => $dyeingCylinderTypeCode,
            'dyeingMachineModelCode' => $dyeingMachineModelCode,
            'iotSerialNumber'        => $iotSerialNumber,
            'pipelineNumber'         => $pipelineNumber,
            'remark'                 => $remark,
            'stationNumber'          => $stationNumber,
            'status'                 => $status,
            'typeCode'               => $typeCode,
            'workshopId'             => $workshopId,
            'workshopName'           => $workshopName
        ]);
        }


    public function configInfo(
        $deviceId = '',
        $machineCode = '',
        $configTypeCode = 'craftParamConfig',
        $groupCode = "CRAFT_TYPE",
        $searchName = "",
        $pageNo = "1",
        $pageSize = '20'
    ) {
        return $this->postJson('dyeing-nest-control/base/config-info/query', [
            'deviceId'       => $deviceId,
            'machineCode'    => $machineCode,
            'configTypeCode' => $configTypeCode,
            'groupCode'      => $groupCode,
            'searchName'     => $searchName,
            'pageNo'         => $pageNo,
            'pageSize'       => $pageSize
        ]);
        }

    }