<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-06-06 12:32:50
 * @Description: 菜单接口
 * 
 */
namespace zhijingfeisuo\WorkOrder;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {

    public function delete($order_id)
        {
        return $this->_DELETE("dyeing-web/production/work-order/delete-all-associated-by-id/{$order_id}");
        }
    public function get($order_id)
        {
        return $this->_GET("dyeing-web/production/work-order/get-single-work-order/{$order_id}");
        }
    public function icCardScan($icCard, $deviceId, $machineCode = '', $isCreate = 0)
        {
        return $this->postJson("dyeing-web/product/ic-card/scan", [
            'deviceId'    => $deviceId,
            'icCard'      => $icCard,
            'machineCode' => $machineCode,
            'isCreate'    => $isCreate,
        ]);
        }

    public function orderList($icCard, $startDate, $endDate, $pageNo, $pageSize, $other_param = [])
        {
        $param = [
            'automatic'               => "",
            'calicoName'              => "",
            'clientCode'              => "",
            'clientName'              => "",
            'craftCode'               => "",
            'craftMaxTempSortType'    => "",
            'craftName'               => "",
            'craftPracticalTimeGt'    => "",
            'deviceName'              => "",
            'endDate'                 => $endDate,
            'icCard'                  => $icCard,
            'maxTempHoldTimeSortType' => "",
            'numAndColor'             => "",
            'overTimeSortType'        => "",
            'pageNo'                  => 1,
            'pageSize'                => 20,
            'queryTimeFlag'           => 2,
            'startDate'               => $startDate,
            'workOrderNo'             => ""
        ];
        $param = array_merge($param, $other_param);
        return $this->postJson("dyeing-nest-control/report/history-order/query/order-list", $param);
        }


    public function info($icCard)
        {
        return $this->postJson("dyeing-web/api/v1/work-order", [
            'orgCode' => $this->app['config']->get('orgCode'),
            'icCard'  => $icCard,
        ]);
        }
    public function saveOrUpdate(
        $machineConfigDTO,
        $orderDTO,
        $workOrderCraftDTO,
        $workOrderDTO,
        $icCardDTO,
        $workOrderFacilityDTO = [],
        $workOrderDeviceRunParams = [],
        $craftCodes = [],
        $issue = 0,
        $run = 0
    ) {
        return $this->postJson("dyeing-web/production/work-order/save-or-update", [
            'craftCodes'               => $craftCodes,
            'machineConfigDTO'         => $machineConfigDTO,
            'orderDTO'                 => $orderDTO,
            'workOrderCraftDTO'        => $workOrderCraftDTO,
            'workOrderDTO'             => $workOrderDTO,
            'workOrderFacilityDTO'     => $workOrderFacilityDTO,
            'workOrderDeviceRunParams' => $workOrderDeviceRunParams,
            'icCardDTO'                => $icCardDTO,
            'issue'                    => $issue,
            'run'                      => $run,
        ]);
        }
    }