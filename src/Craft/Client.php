<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-20 09:53:45
 * @Description: 工艺接口
 * 
 */

namespace zhijingfeisuo\Craft;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {


    public function query(
        $code = '',
        $name = '',
        $status = "",
        $pageNo = 1,
        $pageSize = 1000,
        $modelCode = '',
        $craftType = 1,
        $typeCode = ""
    ) {
        return $this->postJson('dyeing-web/base/craft/query', [
            'code'      => $code,
            'name'      => $name,
            'status'    => $status,
            'pageNo'    => $pageNo,
            'pageSize'  => $pageSize,
            'modelCode' => $modelCode,
            'craftType' => $craftType,
            'typeCode'  => $typeCode,
        ]);
        }

    public function craftCompose($craftIdList, $deviceId, $icCardCode, $icCardWithCraft = false, $code = '', $machineCode = '')
        {

        return $this->_GET('dyeing-web/base/craft/craft-compose', [
            'craftIdList'     => $craftIdList,
            'deviceId'        => $deviceId,
            'icCardWithCraft' => $icCardWithCraft,
            'code'            => $code,
            'machineCode'     => $machineCode,
            'icCardCode'      => $icCardCode,
        ]);

        }

    }