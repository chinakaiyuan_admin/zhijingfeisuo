<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-24 14:44:04
 * @Description: 工艺接口
 * 
 */

namespace zhijingfeisuo\User;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {
    public function exSendSMSForResetPassword($mobile)
        {
        return $this->popMiddleware('access_token')->_GET("uac/password/exSendSMSForResetPassword", [
            'clientId' => $this->app['config']->get('clientId'),
            'mobile'   => $mobile,
        ]);
        }
    public function exValidateSMSToResetPassword($smsCode, $mobile, $password)
        {
        return $this->popMiddleware('access_token')->postJson('uac/password/exValidateSMSToResetPassword', [
            'clientId'        => $this->app['config']->get('clientId'),
            'confirmPassword' => $password,
            'loginPassword'   => $password,
            'mobile'          => $mobile,
            'smsCode'         => $smsCode,
        ]);
        }
    public function list(
        $phone = "",
        $deptId = 0,
        $deptLevel = 0,
        $roleName = "",
        $userName = "",
        $userNo = "",
        $workStatus = "",
        $pageNo = 1,
        $pageSize = 20,
        $orgCode = ""
    ) {
        return $this->postJson('dyeing-web/base/user/list', [
            'deptId'     => $deptId,
            'deptLevel'  => $deptLevel,
            'phone'      => $phone,
            'roleName'   => $roleName,
            'userName'   => $userName,
            'userNo'     => $userNo,
            'workStatus' => $workStatus,
            'pageNo'     => $pageNo,
            'pageSize'   => $pageSize,
            'orgCode'    => $orgCode == "" ? $this->app['config']->get('orgCode') : $orgCode,
        ]);
        }
    public function switchWorkStatus($userid)
        {
        return $this->postJson("dyeing-web/base/user/switch-work-status/" . $userid);
        }

    public function remove($userid)
        {
        return $this->postJson("dyeing-web/base/user/remove/" . $userid);
        }
    public function update(
        $userId,
        $phone = '',
        $roleCodes = [],
        $userName = '',
        $userNo = '',
        $departmentId = [["0"]],
        $deptList = [['deptLevel' => 0, 'deptId' => 0]],
    ) {
        return $this->postJson('dyeing-web/base/user/update', [
            'departmentId' => $departmentId,
            'phone'        => $phone,
            'userId'       => $userId,
            'roleCodes'    => $roleCodes,
            'userName'     => $userName,
            'userNo'       => $userNo,

            'deptList'     => $deptList,
            'orgCode'      => $this->app['config']->get('orgCode'),
        ]);
        }

    public function save(
        $userName,
        $userNo,
        $phone,
        $roleCodes = [],
        $departmentId = [[0]],
        $deptList = [['deptLevel' => 0, 'deptId' => 0]],
        $isHead = false
    ) {
        return $this->postJson('dyeing-web/base/user/save', [
            'departmentId' => $departmentId,
            'phone'        => $phone,
            'roleCodes'    => $roleCodes,
            'userName'     => $userName,
            'userNo'       => $userNo,
            'orgCode'      => $this->app['config']->get('orgCode'),
            'isHead'       => $isHead,
            'deptList'     => $deptList
        ]);
        }
    }