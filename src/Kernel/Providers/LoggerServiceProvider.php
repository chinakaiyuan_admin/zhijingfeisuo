<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-17 12:18:12
 * @Description: 
 * 
 */
namespace zhijingfeisuo\Kernel\Providers;

use Pimple\ServiceProviderInterface;
use zhijingfeisuo\Kernel\Services\Logger;
use Pimple\Container;

class LoggerServiceProvider implements ServiceProviderInterface
    {
    /**
     * Registers services on the given container.
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param \Pimple\Container $pimple A container instance
     */
    public function register(Container $pimple)
        {
        isset($pimple['logger']) || $pimple['logger'] = function ($app)
            {
            return new Logger($app);
            };
        }
    }