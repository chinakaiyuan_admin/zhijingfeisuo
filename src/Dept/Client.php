<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-23 15:13:31
 * @Description: 工艺接口
 * 
 */

namespace zhijingfeisuo\Dept;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {

    public function tree()
        {
        return $this->_GET('dyeing-web/base/dept/tree/all');
        }

    }