<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-14 15:44:20
 * @Description: 菜单接口
 * 
 */
namespace zhijingfeisuo\Menu;

use zhijingfeisuo\Kernel\Client as BaseClient;

class Client extends BaseClient
    {

    public function getMenuList()
        {
        return $this->postJson('dyeing-web/base/menu/get-menu-list', ['clientId' => $this->app['config']->get('clientId')]);
        }
    public function getOrgList()
        {
        return $this->postJson('dyeing-web/base/menu/get-org-list');
        }

    }