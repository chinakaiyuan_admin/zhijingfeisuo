<?php
/*
 * @Author: 孙开源 && sunkaiyuan@namenode.cn
 * @Date: 2023-04-12 16:45:39
 * @LastEditors: 孙开源 && sunkaiyuan@namenode.cn
 * @LastEditTime: 2023-04-23 15:48:00
 * @Description: 
 * 
 */
namespace zhijingfeisuo;

use zhijingfeisuo\Kernel\Support\Collection;
use Pimple\Container;

/** 
 * @property \zhijingfeisuo\Menu\Client $Menu
 * @property \zhijingfeisuo\Craft\Client $Craft
 * @property \zhijingfeisuo\Device\Client $Device
 * @property \zhijingfeisuo\WorkOrder\Client $WorkOrder
 * @property \zhijingfeisuo\Dept\Client $Dept
 * @property \zhijingfeisuo\Role\Client $Role
 * @property \zhijingfeisuo\User\Client $User
 * 
 * @property \zhijingfeisuo\Kernel\Client $client
 * @property \zhijingfeisuo\Kernel\Services\AccessToken $access_token
 * @property \zhijingfeisuo\Kernel\Services\Cacher $cacher
 * @property \zhijingfeisuo\Kernel\Services\Logger $logger
 * @property \zhijingfeisuo\Kernel\Services\Interceptor $interceptor
 */
class Application extends Container
    {
    //默认值
    protected $_defaultconfig = [
        'clientId'     => "",
        'orgCode'      => "",
        'loginAccount' => "",
        'password'     => "",
        'log'          => [
            'enable' => false
        ],
    ];
    /**
     * @var array
     */
    protected $providers = [

        Menu\ServiceProvider::class,
        Craft\ServiceProvider::class,
        Device\ServiceProvider::class,
        WorkOrder\ServiceProvider::class,
        Dept\ServiceProvider::class,
        Role\ServiceProvider::class,
        User\ServiceProvider::class,

        Kernel\Providers\LoggerServiceProvider::class,
        Kernel\Providers\RequestServiceProvider::class,
        Kernel\Providers\AccessTokenServiceProvider::class,
        Kernel\Providers\InterceptorServiceProvider::class,
        Kernel\Providers\CacherServiceProvider::class,
    ];


    /**
     * Application constructor.
     *
     * @param array $config
     * @param array $values
     */
    public function __construct($config = [], array $values = [])
        {
        parent::__construct($values);

        $this['config'] = function () use ($config)
            {
            return new Collection(array_merge($this->_defaultconfig, $config));
            };

        foreach ($this->providers as $provider) {
            $this->register(new $provider());
            }
        }

    public static function make($name, $config)
        {
        return (new self($config))->$name;
        }
    public static function __callStatic($name, $arguments)
        {
        return self::make($name, ...$arguments);
        }
    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
        {
        return $this[$name];
        }
    }